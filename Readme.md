# Installation
## R part

First install **R**, then open start R (as root, if you want to
install your packages system-wide).

    install.packages("tikzDevice")
    install.packages("remotes")
    require(remotes)
    install_gitlab("lur/lur",subdir="packageluR")
    require(luR)
    #this is where libluR.so (or .dylib/.dll) is located: 
    paste0(path.package("luR"),"/libs")
    
## LuaLaTeX part

Now you can either copy/link the generated dynamic library to where
TeX's kpathsea will find it. But it is more convenient to tell
Lua and LuaLaTeX where to find this package within R's site structure.
This also allows for easier updates.
To achieve this, edit
luRconfig.lua as follows (again .dll for windows/.dyblib for Mac OS):

    package.cpath = package.cpath .. "PATH_FROM_ABOVE/?.so" 
    
If you fail to do so, libluR.sty will try to create that file in your
current working directory by calling `Rscript`.
    
Now copy to the files luRconfig.lua and luR.sty to your TEXMFHOME
(usually /home/myusername/texmf) under, e.g., 
`tex/lualatex/luR`. That is the files are now at (e.g.)
`/home/myusername/texmf/tex/lualatex/luR`.

If you do not want to keep the `luR` package in general, but only need
it for some single .tex file, than it is also fine to copy the two files
to the directory of this tex file only. This also works for the shared
library from R, but keep in mind, that this library heavily depends on
your R installation.

# Usage

The most important part is that you have to call `lualatex` with the
`--shell-escape` option. Thus be careful what documents you process
with LuR as `lualatex --shell-escape` allows arbitrary command execution.
As an example, you can try to compile the manual:

    lualatex --shell-escape luRmanual.tex
    
After doing so, further examples can be found in the manual.
