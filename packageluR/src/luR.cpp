#include <RInside.h>
#include <lua.hpp>

RInside *RI;

extern "C" {
static int l_initR(lua_State *L){
  RI = new RInside(); // create an embedded R instance
  return 0;
}

static int l_evalQ (lua_State *L){
  const char* d = luaL_checkstring(L, 1);  //get argument
  
  RI->parseEvalQ(d);//eval with no return
  return 0;
}

int sexpPush(lua_State *L, SEXP x, bool vAsTable = false){
  int nres = 1;
  try {//TODO: try with mostly C-Version of R-API is quite useless
    switch(TYPEOF(x)){
      case LGLSXP: {
        Rcpp::LogicalVector v = x;
        int n = v.length();
        vAsTable = vAsTable && n > 1;
        if (vAsTable) {
          lua_createtable (L,n,0);
        }
        for (int i=0;i<v.length();i++){
          lua_pushboolean(L,v[i]);
          if (vAsTable) {
            lua_rawseti(L,-2,i+1);
          }
        }
        if (vAsTable){
          nres = 1;
        } else {
          nres = n;
        }
      } break;
      case INTSXP: {
        Rcpp::IntegerVector v = x;
        int n = v.length();
        vAsTable = vAsTable && n > 1;
        if (vAsTable) {
          lua_createtable (L,n,0);
        }
        for (int i=0;i<v.length();i++){
          lua_pushinteger(L,v[i]);
          if (vAsTable) {
            lua_rawseti(L,-2,i+1);
          }
        }
        if (vAsTable){
          nres = 1;
        } else {
          nres = n;
        }
      } break;
      case REALSXP: {
        Rcpp::NumericVector v = x;
        int n = v.length();
        vAsTable = vAsTable && n > 1;
        if (vAsTable) {
          lua_createtable (L,n,0);
        }
        for (int i=0;i<v.length();i++){
          lua_pushnumber(L,v[i]);
          if (vAsTable) {
            lua_rawseti(L,-2,i+1);
          }
        }
        if (vAsTable){
          nres = 1;
        } else {
          nres = n;
        }
      } break;
      case STRSXP:  {
        Rcpp::CharacterVector v = x;
        int n = v.length();
        vAsTable = vAsTable && n > 1;
        if (vAsTable) {
          lua_createtable (L,n,0);
        }
        for (int i=0;i<v.length();i++){
          lua_pushstring(L,v[i]);
          if (vAsTable) {
            lua_rawseti(L,-2,i+1);
          }
        }
        if (vAsTable){
          nres = 1;
        } else {
          nres = n;
        }
      } break;
      case VECSXP:  {
        Rcpp::List l = x;
//         std::cout << l.length() << " " <<Rf_length(x) <<std::endl;
        lua_createtable (L,l.length(),0);
        for (int i=0;i<l.length();i++){
//           std::cout << i << std::endl;
          sexpPush(L,(SEXP) l[i],true);
          lua_rawseti(L,-2,i+1);
        }
        nres = 1;
      } break;
      default: {
        x = Rf_asChar(x);
        lua_pushstring(L,CHAR(x));
      } break;
    }
  } catch (Rcpp::not_compatible err) {
      lua_pushstring(L, (new std::string(err.what()))->c_str());
  }
  return nres;
}

static int l_eval (lua_State *L){
  const char* d = luaL_checkstring(L, 1);  //get argument
  
  SEXP ans;
  
  RI->parseEval(d,ans);//eval and return to ans
  
  return sexpPush(L,ans);
}


static int l_passString(lua_State *L){//avoids escaping
  const char* name = luaL_checkstring(L, 1); //global name in R
  const char* str = luaL_checkstring(L, 2); 
  
  (*RI)[name] = str;
  
  return 0;
}
  
static int l_parse (lua_State *L){
  const char* d = luaL_checkstring(L, 1);  // get argument
  SEXP dSexp = R_NilValue;
  int i;
  ParseStatus stat;
  
  PROTECT(dSexp = Rf_allocVector(STRSXP, 1));
  SET_STRING_ELT(dSexp, 0, Rf_mkChar(d));
  
  PROTECT(R_ParseVector(dSexp, -1, &stat, R_NilValue));
  switch (stat){
    case PARSE_OK:
      lua_pushstring(L, (new std::string("OK"))->c_str());
      break;
    case PARSE_INCOMPLETE:
      lua_pushstring(L, (new std::string("INCOMPLETE"))->c_str());
      break;
    default:
      lua_pushstring(L, (new std::string("ERROR"))->c_str());
      break;
  }
  UNPROTECT(2);
  return 1;
}

static const struct luaL_Reg libluR [] = {
  {"init", l_initR},
  {"evalQ", l_evalQ},
  {"eval", l_eval},
  {"passString",l_passString},
  {"parse",l_parse},
  {NULL, NULL}  /* sentinel */
};

LUALIB_API int luaopen_libluR (lua_State *L) {
  lua_newtable(L);
  luaL_setfuncs(L, libluR, 0);
  return 1;
}
}





