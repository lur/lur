libluR.so:	luR.Rcheck
	cp luR.Rcheck/luR/libs/libluR.so .
	
rpackage:
	R CMD build packageluR
	
luR.Rcheck: rpackage
	R CMD check *.tar.gz

clean:
	rm -f *.log *.out *.aux Rplots.* pnorm.* oneto15.* luR*tar.gz
	rm -rf  luR.Rcheck

cleaner: clean
	rm -f libluR.so
	
manual:	libluR.so
	lualatex --shell-escape luRmanual.tex
